/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import db.Config;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author techs
 */
public class M_Buku211011401309 {
  public class Ins_Buku211011401309 {
    private String kode_buku, judul_buku, pengarang;
    
    public Ins_Buku211011401309(String kode_buku, String judul_buku, String pengarang) {
      this.kode_buku = kode_buku;
      this.judul_buku = judul_buku;
      this.pengarang = pengarang;
    }
    
    public String getKode_buku() {
      return kode_buku;
    }
    
    public String getJudul_buku() {
      return judul_buku;
    }
    
    public String getPengarang() {
      return pengarang;
    }
  }
    
  public String create(String kode_buku, String judul_buku, String pengarang)
  {
    String response = "";
    
    String sql  = "INSERT INTO tbbuku_211011401309 (kode_buku, judul_buku, pengarang) VALUES ('"+ kode_buku +"', '"+ judul_buku +"', '"+ pengarang +"')";
    
    try {
      Statement state = Config.configDB().createStatement();
      state.executeUpdate(sql);
      
      state.close();
      response = "Data buku berhasil ditambahkan!";
    } catch (Exception e) {
      System.err.println(e.getMessage());
      response = "Data buku gagal ditambahkan!";
    }
    
    return response;
  }
  
  public String destroy(String kode_buku) {
    String response = "";
    String selectQuery = "SELECT * FROM tbbuku_211011401309 WHERE kode_buku = '" + kode_buku + "'";
    
    try {
      Statement selectStatement = Config.configDB().createStatement();
      ResultSet resultSet = selectStatement.executeQuery(selectQuery);

      if (resultSet.next()) {
        String deleteQuery = "DELETE FROM tbbuku_211011401309 WHERE kode_buku = '" + kode_buku + "'";
        Statement deleteStatement = Config.configDB().createStatement();
        deleteStatement.executeUpdate(deleteQuery);
        deleteStatement.close();

        response = "Data buku berhasil dihapus!";
      } else {
        response = "Buku tidak ditemukan, Data buku gagal dihapus!";
      }

      resultSet.close();
      selectStatement.close();
    } catch (Exception e) {
      System.err.println(e.getMessage());
      response = "Data buku gagal dihapus!";
    }
    
    return response;
  }
  
  public ArrayList<M_Buku211011401309.Ins_Buku211011401309> dataTable(String orderBy)
  {
    ArrayList<Ins_Buku211011401309> list_buku = new ArrayList<>();
    ResultSet rs = null;

    try {
      String sql = "SELECT * FROM tbbuku_211011401309 ORDER BY " + orderBy;
      Statement state = Config.configDB().createStatement();
      rs = state.executeQuery(sql);

      while (rs.next()) {
        String kode_buku = rs.getString("kode_buku");
        String judul_buku = rs.getString("judul_buku");
        String pengarang = rs.getString("pengarang");
        
        Ins_Buku211011401309 buku = new Ins_Buku211011401309(kode_buku, judul_buku, pengarang);
        list_buku.add(buku);
      }
    } catch (Exception e) {
      System.err.println("Transaksi tidak tersedia");
      System.err.println("" + e.getMessage());
    }

    return list_buku;
  }
}
