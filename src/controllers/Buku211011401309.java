/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.M_Buku211011401309;

import views.Buku_Form;
import views.Buku_Data;

/**
 *
 * @author techs
 */
public class Buku211011401309 {
  private Buku_Form view;
  private M_Buku211011401309 model;
  private Thread someThread;
  
  public Buku211011401309(Buku_Form view)
  {
    this.view = view;
    this.model = new M_Buku211011401309();
  }
  
  public static void main(String[] args)
  {
    new Buku_Form().setVisible(true);
  }
  
  public void togglePanel(Boolean type)
  {
    view.setVisible(type);
  }
  
  public void lihat_buku(String orderby)
  {
    new Buku_Data(orderby).setVisible(true);
  }
  
  public void create(String kode_buku, String judul_buku, String pengarang)
  {
    String response = model.create(kode_buku, judul_buku, pengarang);
    JOptionPane.showMessageDialog(view, response);
  }
  
  public void destroy(String kode_buku)
  {
    String response = model.destroy(kode_buku);
    JOptionPane.showMessageDialog(view, response);
  }
  
  public ArrayList<M_Buku211011401309.Ins_Buku211011401309> dataTable(String orderBy)
  {
    return model.dataTable(orderBy);
  }
}
