package db;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author techs
 */
public class Config {
  private static Connection mysqlconfig;
  
  public static Connection configDB() throws SQLException {
    try {
      String url = "jdbc:mysql://localhost:3306/dbbuku_211011401309";
      String user = "root";
      String password = "";
      DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
      mysqlconfig = DriverManager.getConnection(url, user, password);
    } catch (Exception e) {
      System.err.println("Koneksi gagal " + e.getMessage());
    }
    
    return mysqlconfig;
  }
}
